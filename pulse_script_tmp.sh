#!/bin/bash
  
cd ~

git clone https://github.com/neutrinolabs/pulseaudio-module-xrdp.git

cd pulseaudio-module-xrdp/
scripts/install_pulseaudio_sources_apt.sh
sudo apt-get install libpulse-dev
./bootstrap && ./configure PULSE_DIR=~/pulseaudio.src
make
sudo make install
