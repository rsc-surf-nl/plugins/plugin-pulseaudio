#!/bin/bash
  
if [ -d "/home/$(whoami)/pulseaudio.src" ]
then
    echo "Directory exists."
else
    cd /home/
    sudo -u $(whoami) /tmp/pulse_script_tmp.sh
fi